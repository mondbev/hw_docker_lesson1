#!/usr/bin/env bash

# pull container named nginx
docker pull nginx

# inspect the container and write down the hostname
DOCKER_CONTAINER_HOSTNAME="$(docker image inspect nginx | grep ContainerConfig -A1 | tail -n 1 | awk '{print $2}' | tr -d '\"')"
DOCKER_CONTAINER_HOSTNAME="${DOCKER_CONTAINER_HOSTNAME::-1}" #remove ,

# check the version of image and verify it's the latest
DOCKER_IMAGE_VERSION="$(docker image ls nginx | grep nginx | awk '{print $2}')"
[[ "$DOCKER_IMAGE_VERSION" =~ "latest" ]] && echo "Latest version" || echo "$DOCKER_IMAGE_VERSION"

# check what is main command it is running
docker image inspect nginx | grep CMD

# list all running containers
docker container ls --all

# run a container of busybox instance
docker pull busybox
docker run --name busyb_container busybox

# check if container is running, if not - why?
DOCKER_CONTAINER_STATUS="$(docker container ls --all | grep busybox | awk '{print $7}')"
[[ "$DOCKER_CONTAINER_STATUS" =~ "Exited" ]] && echo "Not running: $DOCKER_CONTAINER_STATUS" || echo "$DOCKER_CONTAINER_STATUS"

# create nginx container in daemon mode
docker run --name nginx_container -d nginx

# inspect the container and write down it's IP
DOCKER_CONTAINER_IP="$(docker container inspect nginx_container | grep IPAddress | tail -n1 | awk '{print $2}' | tr -d '\"' | tr -d ',')"
curl "$DOCKER_CONTAINER_IP"

# check container logs
docker container logs nginx_container

# attach the container to a shell
docker container exec -it nginx_container /bin/sh

# check if container is running, if not - why?
DOCKER_CONTAINER_STATUS="$(docker container ls --all | grep nginx_container | awk '{print $7}')"
[[ "$DOCKER_CONTAINER_STATUS" =~ "Up" ]] && echo "Running: $DOCKER_CONTAINER_STATUS" || echo "$DOCKER_CONTAINER_STATUS"

# start container by id
docker container start $(docker container ls --all | grep nginx_container | awk '{print $1}')

# check stats of new started container
docker container stats $(docker container ls | grep nginx_container | awk '{print $1}')

# exec /bin/sh command on container
docker container exec -it nginx_container /bin/sh